<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\PayrollController;
use App\Http\Controllers\YourProfileController;
use App\Http\Controllers\UserAttendanceController;
use App\Http\Controllers\UserPayrollController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 /*
	if 1 page only dont make group
	if multiple page or route please make a group
 */




// Login
Route::get('/', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'check_login'])->name('auth.check');
Route::get('/logout', [AuthController::class, 'logoutUser'])->name('auth.logout');

// admin modules
Route::group(['middleware' => ['auth', 'AuthAdmin']], function(){
// admin Dashboard
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
Route::get('/dashboard/quote', [DashboardController::class, 'quote'])->name('dashboard.quote');

// Employee management
Route::group(['prefix' => 'employee', 'as' => 'employee.'], function(){
	Route::get('', [EmployeeController::class, 'index'])->name('index');
	Route::get('/add', [EmployeeController::class, 'add'])->name('add');
	Route::post('/update', [EmployeeController::class, 'updateEmployee'])->name('update');
	Route::post('/save', [EmployeeController::class, 'addEmployee'])->name('save');
	Route::get('/past', [EmployeeController::class, 'passEmployee'])->name('pass_employee');
	Route::delete('/delete/{id?}', [EmployeeController::class, 'deleteEmployee'])->name('delete');
	Route::get('/activate/{id?}', [EmployeeController::class, 'activateEmployee'])->name('activate');
	Route::get('/deactivate/{id?}', [EmployeeController::class, 'deactivateEmployee'])->name('deactivate');
});
// Attendance
Route::get('/attendance', [AttendanceController::class, 'index'])->name('attendance');
//loan
Route::get('/loan', [LoanController::class, 'index'])->name('loan');
//payroll
Route::get('/payroll', [PayrollController::class, 'index'])->name('payroll');
});




// User account modules
Route::group(['middleware' => ['auth', 'AuthUser']], function(){
	Route::group(['prefix' => 'account', 'as' => 'account.'], function(){
		Route::get('', [AccountController::class, 'index'])->name('index');
	});

//your profile controller
Route::group(['prefix' => 'user/profile', 'as' => 'user/profile.'], function(){
		Route::get('', [YourProfileController::class, 'index'])->name('index');
	});
//user attendance
Route::group(['prefix' => 'user/attendance', 'as' => 'user/attendance.'], function(){
		Route::get('', [UserAttendanceController::class, 'index'])->name('index');
		Route::get('list', [UserAttendanceController::class, 'list'])->name('list');
		Route::get('insert', [UserAttendanceController::class, 'insert'])->name('insert');
		Route::post('timeInAm/', [UserAttendanceController::class, 'timeInAm'])->name('timeInAm');
		Route::post('timeOutAm/', [UserAttendanceController::class, 'timeOutAm'])->name('timeOutAm');
		Route::post('timeInPm/', [UserAttendanceController::class, 'timeInPm'])->name('timeInPm');
		Route::post('timeOutPm/', [UserAttendanceController::class, 'timeOutPm'])->name('timeOutPm');
		Route::get('outdoorList/{id?}', [UserAttendanceController::class, 'outdoorList'])->name('outdoorList');
		Route::get('outdoorTimeOut', [UserAttendanceController::class, 'outdoorTimeOut'])->name('outdoorTimeOut');
		Route::get('outdoorTimeOut', [UserAttendanceController::class, 'outdoorTimeOut'])->name('outdoorTimeOut');
		Route::get('editOutdoorTimeOut', [UserAttendanceController::class, 'editOutdoorTimeOut'])->name('editOutdoorTimeOut');
		Route::get('editOutdoorTimeIn', [UserAttendanceController::class, 'editOutdoorTimeIn'])->name('editOutdoorTimeIn');
		Route::get('outdoorReason', [UserAttendanceController::class, 'outdoorReason'])->name('outdoorReason');
		Route::post('workplace', [UserAttendanceController::class, 'workPlace'])->name('workplace');
		Route::post('addReport', [UserAttendanceController::class, 'addReport'])->name('addreport');
		Route::post('addReason', [UserAttendanceController::class, 'addreason'])->name('addreason');

	});
//user payroll
Route::group(['prefix' => 'user/payroll', 'as' => 'user/payroll.'], function(){
		Route::get('', [UserPayrollController::class, 'index'])->name('index');
	});



});
