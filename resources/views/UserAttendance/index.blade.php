@extends('Layout.app_user')

@section('content')
<style>
    input[type="time"]::-webkit-calendar-picker-indicator {
    background: none;
}

.iconss:hover{
    background-color: skyblue;
    transition: 1s;
    cursor: pointer;
}
.time_in_am:hover:before{
    content: "time in AM";
    color:white;
}
.time_out_am:hover:before{
    content: "time out AM";
    color:white;
}
.time_in_pm:hover:before{
    content: "time in PM";
    color:white;
}
.time_out_pm:hover:before{
    content: "time out PM";
    color:white;
}

.outdoor:hover:before{
  content: "Outdoor DTR" ;
  color: white;
}
.status:hover:before{
  content: "Workplace";
  color: white;
}
.report:hover:before{
  content: "Report";
  color: white;
}
.reason:hover:before{
  content: "Reason";
  color: white;
}
</style>
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-primary">Attendance</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Attendance</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="background: #343A40; color: white;"><h3>Employee Attendance Report</h3></div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-3">
                            <label>Employee</label>
                            <input class="form-control py-2" type="text" placeholder="Employee" id="employee" name="employee">
                        </div>

                        <div class="col-sm-3">
                            <label>Year</label>
                            <input class="form-control py-2" type="number" placeholder="Year" id="year" name="year">
                        </div>

                        <div class="col-sm-3">
                            <label>Month</label>
                            <select class="form-control" id="month" name="month">
                                <option value="">-- select month --</option>
                                <option disabled>------------------</option>
                                @foreach(monthsOption() as $val => $data)
                                <option value="{{ $val }}">{{ $data }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3 justify-content-end d-flex align-items-end">
                            <button class="btn btn-success btn_submit mx-3">Download <i class="fas fa-download"></i></button>
                            <button class="btn btn-primary btn_submit" id="show_attendance" type="button">Generate Table</button>
                        </div>
                    </div>
                    <div style="overflow-x: auto">
                        <table class="table table-hover" style="font-size: 11pt">
                            <thead>
                            <tr>
                                <th>Firstname</th>
                            <th class="text-center" style="width: 10%">Date</th>
                            <th class="text-center" style="width: 10%">Day</th>

                            <th class="text-center iconss time_in_am" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/1QrpIt8klO-WSXOGLkFTnYnvTZF46S0wu-x4lZVMs.png') }}" height="40px"></th>

                            <th class="text-center iconss time_out_am" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/1kG4cR663ocePqMwr7r17USRppu0ucoyZmQHMAy36.png') }}" height="40px"></th>

                            <th class="text-center iconss time_in_pm" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/1Bu-XdG8Ap6mxfk16QCFn6wpAojmOT2bjp6KwXxgP.png') }}" height="40px"></th>

                            <th class="text-center iconss time_out_pm" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/11z7o1b9JusWokC7Y0kJFDNJy0jMFm3WGqiE_pq7f.png') }}" height="40px"></th>

                            <th class="text-center iconss outdoor" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/1T8ScrPPWtw0qicGNdKa9AqXibPlXO_4hsZhgmO79.png') }}" height="40px"></th>

                            <th class="text-center iconss status" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/1STS7CfBI97nL1_Qof3BoUrfeMgbtcZhBCgGHDaJQ.png') }}" height="40px"></th>

                            <th class="text-center iconss report" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/1FRJHJrukWR8UZodx4lhW1gf1PMcvxx5uH2B4aQdF.png') }}" height="40px"></th>

                            <th class="text-center iconss reason" style="width: 10%; padding: 0px; padding-bottom: 5px"><img src="{{ asset('img/icons/15GG6meRaKIr0hNwWrPdeF03o_MbHa6Lz2YrTAvkY.png') }}" height="40px"></th>
                            </tr>
                            </thead>
                            <tbody id="attendance_tbl_data">
                                   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="outdoor_form" action="" class="needs-validation" novalidate>
 <div class="modal fade" role="dialog" id="modal_outdoor" style="max-height: 150vh">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Outdoor DTR: <b> {{Auth::user()->first_name.' '.Auth::user()->last_name}}</b><br><i id="date_modal"></i>
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
              <div class="modal-body form-group" style="overflow-x: auto; overflow-y: auto;">
                <input type="hidden" name="attendance_id_outdoor" id="attendance_id_outdoor">
                <table class="table table-hover" style="font-size: 11pt">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Time out</th>
                                        <th>Time in</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="outdoor_tbl_data">
                                   
                                </tbody>
                    </table>
              </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-right">
                <button type="button" class="btn btn-success" id="add_outdoor_btn" style="width: 84px; margin-right: 40px">Add</button>
                </div>
            </div>
          
        </div>
      </div>
    </div>
</form>

<!-- report modal -->
 <div class="modal fade" role="dialog" id="report_modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Report Modal
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form id="report_modal_form" class="needs-validation" novalidate="">
            <div class="row">
              <div class="col-sm-12">
              <div class="col-sm-12">
              <label>Report Message</label>
              <textarea class="form-control" rows="3" placeholder="Enter report message here" id="report_message" name="report_message"></textarea>
              <div class="invalid-feedback" id="err_report_message"></div>
              </div>
              <div class="col-sm-12 mt-2">
                <input type="hidden" name="attendance_id_report" id="attendance_id_report">
                <input type="file" name="report_file" id="report_file" class="form-control" hidden="">
                <button class="btn btn-info" type="button" onclick="upload_file()"><i class="fa-solid fa-plus"></i>Upload Document Here</button>
              </div>
            </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-danger px-2" type="button" onclick="clear_report_modal()">Clear</button>
            <button class="btn btn-success" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

     <div class="modal fade" role="dialog" id="reason_modal">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-title">
                Reason for absent
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <form id="reason_modal_form" class="needs-validation" novalidate="">
            <div class="row">
              <div class="col-sm-12">
              <label>Reason Message</label>
              <input type="hidden" name="attendance_id_reason" id="attendance_id_reason">
              <textarea class="form-control" rows="3" placeholder="Enter reason message here" id="reason_message" name="reason_message"></textarea>
              <div class="invalid-feedback" id="err_reason_message"></div>
              </div>
            </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-danger px-2" type="button" onclick="clear_reason_modal()">Clear</button>
                <button class="btn btn-success" type="submit">Submit</button>
              </div>
            </div>
          </div>
        </div>
@endsection


@section('script')
<script type="text/javascript">
  function reason(id, data){
      $('#attendance_id_reason').val(id);
      $('#reason_message').val(data);
      $('#reason_modal').modal('show');
    }


  $('#reason_modal_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    $.ajax({
        type:"POST",
        url:"{{ route('user/attendance.addreason') }}",
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            swal("Success", response.message, "success");
            $('#reason_modal_form').trigger("reset");
            $('#reason_modal').modal('hide');
            list_user();
         }else{
           showValidator(response.error,'reason_modal_form');
           console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });

function clear_report_modal(){
  $('#report_modal_form').trigger("reset");
}

function clear_reason_modal(){
  $('#reason_modal_form').trigger("reset");
}

function upload_file(){
  $("#report_file").trigger("click");
}

function report(id, report_message){
      $('#report_message').val(report_message);
      $('#report_modal').modal('show');
      $('#attendance_id_report').val(id);
    }
      
$('#report_modal_form').on('submit', function(e){
e.preventDefault();
let form = $("#report_modal_form")[0];
    // Create an FormData object 
var data = new FormData(form);
let formData = $(this).serialize();
$.ajax({
    type:"POST",
    url:"{{ route('user/attendance.addreport') }}",
    data:data,
    enctype: 'multipart/form-data',
    processData: false,  // Important!
    contentType: false,
    dataType:'json',
    beforeSend:function(){
    },
    success:function(response){
     if (response.status == true) {
        swal("Success", response.message, "success");
        $('#report_modal_form').trigger("reset");
        $('#report_modal').modal('hide');
        list_user();

     }else{
      showValidator(response.error,'report_modal_form');
      console.log(response);
     }
    },
    error: function(error){
      console.log(error);
    }
  });
});


list_user();

    function list_user(){
        $.ajax({
            type:'get',
            url:"{{ route('user/attendance.list')}}",
            data:{},
            dataType:'json',
            success:function(response){
                //console.log(response);
            let responseData = response.data;
            let table_output = responseData.map(function(usr){
            let out = '';
            let weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][new Date(usr.attendance_date).getDay()];
    out +='<tr>';
    out +='<td><p style="width: 100px">{{Auth::user()->first_name}}</p></td>';
    out +='<td class="text-center"><input type="text" name="" class="border-0" style="width: 75px" value="'+usr.attendance_date+'"></td>';
    if (weekday == 'Sunday' || weekday == 'Saturday') {
    out +='<td class="text-center"><input type="text" name="" class="text-center border-0 text-danger" style="width: 80px;" value="'+weekday+'"></td>';
    }else{
    out +='<td class="text-center"><input type="text" name="" class="text-center border-0" style="width: 80px;" value="'+weekday+'"></td>';
    }
    out +='<td class="text-center pl-5"><input type="time" name="time_in_am" class="border-0" style="width: 100px" oninput="time_in_am('+ usr.id+', this.value)" value="'+usr.time_in_am+'"></td>';
    out +='<td class="text-center pl-5"><input type="time" name="time_out_am" class="border-0" style="width: 100px" oninput="time_out_am('+ usr.id+', this.value)" value="'+usr.time_out_am+'"></td>';
    out +='<td class="text-center pl-5"><input type="time" name="time_in_pm" class="border-0" style="width: 100px" oninput="time_in_pm('+ usr.id+', this.value)" value="'+usr.time_in_pm+'"></td>';
    out +='<td class="text-center pl-5"><input type="time" name="time_out_pm" class="border-0" style="width: 100px" oninput="time_out_pm('+ usr.id+', this.value)" value="'+usr.time_out_pm+'"></td>';
    out +='<td class="">';
    out +='<button class="btn btn-success btn-sm" style="width: 100px" onclick="out_door(\''+usr.attendance_date+'\', \''+usr.id+'\')">Outdoor</button>';
    out +='</td>';
    out +='<td>';
    out+='<select class="form-control bg-primary text-light" style="width: 100px; height: 32px" id="office_home_id" onchange="office_home(this.value, '+usr.id+')">';
    if (usr.status == 1) {
      out+='<option value="1" selected>Office</option>';
      out+='<option value="2">Home</option>';
  }else{
      out+='<option value="1">Office</option>';
      out+='<option value="2" selected>Home</option>';
  }
    out+='</select>';
    out +='</td>';
    out +='<td class="">';
    out +='<button class="btn btn-info btn-sm" style="width: 100px" onclick="report(\''+usr.id+'\',\''+usr.report_message+'\')">Report</button>';
    out +='</td>';
    out +='<td class="">';
    out +='<button class="btn btn-danger btn-sm" style="width: 100px" onclick="reason(\''+usr.id+'\',\''+usr.reason+'\')">reason</button>';
    out +='</td>';
    out +='</td>';
    out +='</tr>';
    return out;
            }).join('');
                $('#attendance_tbl_data').html(table_output);
            }
        });
    }


    


    function office_home(data, id){
      $.ajax({
          type:"POST",
          url:"{{ route('user/attendance.workplace') }}",
          data:{data: data, id: id},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              // swal("Success", response.message, "success");
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
    }

function out_door(attendance, id){
    $('#date_modal').html(attendance);
    $('#attendance_id_outdoor').val(id);
    $('#modal_outdoor').modal('show');
    $.ajax({
            type:'get',
            url:"{{ route('user/attendance.outdoorList')}}"+'/'+id,
            data:{},
            dataType:'json',
            success:function(response){
                //console.log(response);
            var dtr_outdoor_count = 1;

            let responseData = response.data;
            let table_output = responseData.map(function(usr){
            let out = '';
                out+='<tr>';
                out+='<td>'+dtr_outdoor_count+'</td>'
                out+='<td><input type="time" name="outdoor_time_out" value="'+usr.e_time_out+'" oninput="edit_time_out_outdoor('+usr.id+', this.value)" class="form-control onchange_input"></td>';
                out+='<td><input type="time"  name="outdoor_time_in"  value="'+usr.e_time_in+'" oninput="edit_time_in_outdoor('+usr.id+', this.value)" class="form-control onchange_input"></td>';
                out+='<td><input type="text" name="outdoor_reason" id="outdoor_reason" class="form-control" placeholder="Enter reason here" oninput="reason_outdoor('+usr.id+', this.value)" value="'+usr.outdoor_reason+'"></td>';
                
                out+='<td class="text-center">';
                out+='</td>';
                out+='</tr>';
                dtr_outdoor_count++;
                return out;
            }).join('');
                $('#outdoor_tbl_data').html(table_output);

            }
        });
    }

function reason_outdoor(id, reason){
    $.ajax({
        type:"get",
        url:"{{ route('user/attendance.outdoorReason') }}",
        data:{ attendance_id : id, reason: reason },
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          
         if (response.status == true) {
            
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
}

function edit_time_in_outdoor(id, time_in){
    $.ajax({
        type:"GET",
        url:"{{ route('user/attendance.editOutdoorTimeIn') }}",
        data:{time_in : time_in, outdoor_id : id},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
         if (response.status == true){
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
}


function edit_time_out_outdoor(id, time_out){
    $.ajax({
        type:"GET",
        url:"{{ route('user/attendance.editOutdoorTimeOut') }}",
        data:{time_out : time_out, outdoor_id : id},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
         if (response.status == true){
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
}


var input_counter = 0;
$('#add_outdoor_btn').on('click', function(e){
    let out = '';
    out+='<tr id="outdoor_input_'+input_counter+'">';
    out+='<td>#</td>'
    out+='<td><input type="time" name="outdoor[time_out]" id="outdoor[time_out]" onchange="time_out_outdoor(this.value)" class="form-control onchange_input"></td>';
    out+='<td><input type="time" name="outdoor[time_in]" id="outdoor[time_in]" class="form-control onchange_input"></td>';
    out+='<td class="text-center">';
    out+='<button type="button" class="btn btn-danger" onclick="remove_outdoor_input('+input_counter+');" style="width: 84px">Remove</button>';
    out+='</td>';
    out+='</tr>';
    $('#outdoor_tbl_data').append(out);
    input_counter++;
});



function remove_outdoor_input(id){
    $("#outdoor_input_"+id).addClass('animated zoomOutRight', function(){
       setTimeout(function(){
         $("#outdoor_input_"+id).remove();
       },700);
    });
}

function time_out_outdoor(time_out){

   const attendance_id_outdoor = $('#attendance_id_outdoor').val();
    $.ajax({
        type:"GET",
        url:"{{ route('user/attendance.outdoorTimeOut') }}",
        data:{attendanceId : attendance_id_outdoor, time_out : time_out},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            swal("Success", response.message, "success");
            $('#modal_outdoor').modal('hide');

         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
    }

$('#show_attendance').on('click',function(e){
    e.preventDefault();
    $.ajax({
        type:"GET",
        url:"{{ route('user/attendance.insert')}}",
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
           console.log(response);
         if (response.status == true) {
            swal("Success", response.message, "success");
            list_user();
         }else{
            swal("error", response.message, "error");
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
    });

function time_in_am(id, time){
   $.ajax({
       type:"post",
       url:"{{ route('user/attendance.timeInAm') }}",
       data:{id: id, time: time},
       dataType:'json',
        proccessdata: true,
       beforeSend:function(){
       },
       success:function(response){
         // console.log(response);
        if (response.status == true) {
        }else{            
         console.log(response);
        }
       },
       error: function(error){
         console.log(error);
       }
     });
    }

function time_out_am(id, time){
   $.ajax({
       type:"post",
       url:"{{ route('user/attendance.timeOutAm') }}",
       data:{id: id, time: time},
       dataType:'json',
        proccessdata: true,
       beforeSend:function(){
       },
       success:function(response){
         // console.log(response);
        if (response.status == true) {
        }else{            
         console.log(response);
        }
       },
       error: function(error){
         console.log(error);
       }
     });
    }

function time_in_pm(id, time){
   $.ajax({
       type:"post",
       url:"{{ route('user/attendance.timeInPm') }}",
       data:{id: id, time: time},
       dataType:'json',
        proccessdata: true,
       beforeSend:function(){
       },
       success:function(response){
         // console.log(response);
        if (response.status == true) {
        }else{            
         console.log(response);
        }
       },
       error: function(error){
         console.log(error);
       }
     });
    }

function time_out_pm(id, time){
   $.ajax({
       type:"post",
       url:"{{ route('user/attendance.timeOutPm') }}",
       data:{id: id, time: time},
       dataType:'json',
        proccessdata: true,
       beforeSend:function(){
       },
       success:function(response){
         // console.log(response);
        if (response.status == true) {
        }else{            
         console.log(response);
        }
       },
       error: function(error){
         console.log(error);
       }
     });
    }
</script>
@endsection