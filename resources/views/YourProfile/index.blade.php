@extends('Layout.app_user')
@section('css')
<style type="text/css">
  .card{
    border: solid black 2px;
  }
</style>
@endsection
@section('content')
<div class="content-wrapper">
    @include('Layout.bread', ['title' => 'User Profile', 'data' => [] ])

	<div class="container-fluid">
		
  <div class="container py-5">
    <div class="row">
      <div class="col-lg-4">
        <div class="card mb-4" style="height: 410px;">
          <div class="card-body text-center">
            <img src="{{asset('storage/'.auth()->user()->user_img)}}"
              class="rounded-circle img-fluid" style="width: 200px; height: 200px; border: solid black 2px;">
            <h5 class="my-3">{{ auth()->user()->last_name. ' ' .auth()->user()->first_name. ' ' .auth()->user()->middle_name }}</h5>
            <p class="mb-2">{{ auth()->user()->employment_status }}</p>
            <p class="mb-2">{{ auth()->user()->email_address }}</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">First Name</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{auth()->user()->first_name}}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Middle Name</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->middle_name }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Last Name</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->last_name }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Birthdate</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->birth_date }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Date Employed</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->date_employed }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Employment Status</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->employment_status }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Address</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->address }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <h4 class="mb-0 ml-2 mb-2"> Emergency Contact</h4>
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Full Name</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{auth()->user()->ec_name}}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Relationship to employee</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->ec_relation_to }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Phone Number</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->ec_contact }}</p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <p class="mb-0">Address</p>
              </div>
              <div class="col-sm-9">
                <p class="mb-0">{{ auth()->user()->ec_address }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
	</div>
@endsection