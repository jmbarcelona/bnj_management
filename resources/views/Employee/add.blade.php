@extends('Layout.app_admin')
@section('title', 'Admin Dashboard')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   @include('Layout.bread', ['title' => 'Add Employee', 'data' => ['Add Employee' => '']])
    <!-- /.content-header -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <form action="{{ route('employee.save')}}" id="add_form" enctype="multipart/form-data" novalidate="" class="needs-validation">
            <div class="card">
              <div class="card-header h4 text-light" style="background: #343A40">Fill out the form below</div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-4 form-group mb-3">
                    <label>Lastname</label>
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Enter lastname">
                    <div class="invalid-feedback" id="err_last_name"></div>
                  </div>
                  <div class="col-sm-4 form-group mb-3">
                    <label>Middlename</label>
                    <input type="text" name="middle_name" id="middle_name" class="form-control" placeholder="Enter middlename">
                    <div class="invalid-feedback" id="err_middle_name"></div>
                  </div>
                  <div class="col-sm-4 form-group mb-3">
                    <label>Firstname</label>
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Enter firstname">
                    <div class="invalid-feedback" id="err_first_name"></div>
                  </div>
                  <div class="col-sm-6 form-group mb-3">
                    <label>Birthdate</label>
                    <input type="date" name="birth_date" id="birth_date" class="form-control" placeholder="Enter birthdate">
                    <div class="invalid-feedback" id="err_birth_date"></div>
                  </div>
                  <div class="col-sm-6 form-group mb-3">
                    <label>Date Employed</label>
                    <input type="date" name="date_employed" id="date_employed" class="form-control" placeholder="Enter Date Employed">
                    <div class="invalid-feedback" id="err_date_employed"></div>
                  </div>
                   <div class="col-sm-6 form-group mb-3">
                    <label>Gender</label>
                    <select name="gender" id="gender" class="form-control">
                      <option value="" selected="" disabled="">Select Gender</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                    <div class="invalid-feedback" id="err_gender"></div>
                  </div>
                  <div class="col-sm-6 form-group mb-3">
                    <label>Employment Status</label>
                    <select name="employment_status" id="employment_status" class="form-control">
                      <option value="" selected="" disabled="">Select Employment status</option>
                      <option value="Regular">Regular</option>
                      <option value="Trainee">Trainee</option>
                    </select>
                    <div class="invalid-feedback" id="err_employment_status"></div>   
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Address</label>
                    <textarea class="form-control" id="address" name="address" placeholder="Enter Full Address here" rows="4"></textarea>
                    <div class="invalid-feedback" id="err_address"></div>  <hr> 
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Email</label>
                    <input type="text" name="email_address" id="email_address" class="form-control" placeholder="Enter Email">
                    <div class="invalid-feedback" id="err_email_address"></div>  
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Password</label>
                    <input type="Password" name="password" id="password" class="form-control" placeholder="Enter password">
                    <div class="invalid-feedback" id="err_password"></div> 
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm_pass" id="confirm_pass" class="form-control" placeholder="Confirm password">
                    <div class="invalid-feedback" id="err_confirm_pass"></div> 
                  </div>
                  <div class="col-sm-12 form-group">
                    <label>Upload Profile Image</label>
                    <input type="File" name="user_img" id="user_img" class="form-control-file border">
                    <div class="invalid-feedback" id="err_user_img"></div> 
                    <hr>
                  </div>
                  <div class="col-sm-12 form-group text-center">
                    <label class="h5">------- Emergency Contact -------</label>
                  </div>
                  <div class="col-sm-12 form-group">
                    <label>Full Name</label>
                    <input type="text" name="ec_name" id="ec_name" class="form-control" placeholder="Enter Fullname here">
                    <div class="invalid-feedback" id="err_ec_name"></div> 
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Relationship to employee</label>
                    <input type="text" name="ec_relation_to" id="ec_relation_to" class="form-control" placeholder="Enter Relation to employee">
                    <div class="invalid-feedback" id="err_ec_relation_to"></div> 
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Contact Number</label>
                    <input type="text" name="ec_contact" id="ec_contact" class="form-control" placeholder="Enter Contact here">
                    <div class="invalid-feedback" id="err_ec_contact"></div> 

                  </div>
                  <div class="col-sm-12 form-group">
                    <label>Address</label>
                    <textarea name="ec_address" id="ec_address" class="form-control" placeholder="Enter Address here" rows="4"></textarea>
                    <div class="invalid-feedback" id="err_ec_address"></div> 
                  </div>
                </div>
              </div>
              <div class="card-footer text-right text-light" style="background: #343A40">
                <button type="button" class="px-4 btn btn-secondary" id="clear_btn">clear</button>
                <button type="submit" class="px-4 btn btn-success" id="save_button">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
  $('#clear_btn').on('click', function(){
  $('#add_form')[0].reset();
  });


  $('#add_form').on('submit', function(e){
  e.preventDefault();
  // let formData = $(this).serialize();
  let form = $("#add_form")[0];
    // Create an FormData object 
  var data = new FormData(form);
  let url = $(this).attr('action');
  $.ajax({
      type:"POST",
      url:url,
      data:data,
      enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
         //console.log(response);
       if (response.status === true) {
          swal("Success", response.message, "success");
          $('#add_form')[0].reset();
       }else{
          swal("error", response.message, "error");
        showValidator(response.error,'add_form');
        console.log(response);
       }
      },
      error: function(error){
        console.log(error);
      }
    });

});
</script>
@endsection