@extends('Layout.app_admin')
@section('title', 'Current Employee')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="container-fluid mt-2">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-dark h4">Past Employees</div>
                    <div class="card-body bg-secondary">
                        <div class="row">
                        @foreach($users as $user)
                        <div class="col-sm-3 text-center">
                            <div class="card">
                                <div class="card-body bg-dark">
                                    <div class="col-sm-12 col-md-12">
                                    <label>{{$user->first_name.' '.$user->last_name}}</label>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                    <img src="{{ asset('storage/'.$user->user_img)}}" class="img-fluid" 
                                    style="width: 220px; height: 220px; border: solid white; cursor: pointer" 
                                    onclick="image_shower('{{ asset('storage/'.$user->user_img)}}', '{{$user->first_name.' '.$user->last_name}}', '{{$user->birth_date}}',
                                    '{{$user->date_employed}}', '{{$user->employment_status}}', '{{$user->email_address}}', '{{$user->address}}', '{{$user->employee_position}}',
                                    '{{$user->ec_name}}', '{{$user->ec_relation_to}}', '{{$user->ec_contact}}', '{{$user->ec_address}}',
                                    )">
                                    </div>
                                    <label class="h5 mt-2"> 
                                        @if(!empty($user->employee_position))
                                        <div class="col-sm-12 col-md-12">
                                        {{$user->employee_position}}
                                        </div>
                                        @else
                                        <div class="col-sm-12 col-md-12">
                                        No Position yet
                                        </div>
                                        @endif
                                    </label>
                                    <div>
                                    <div class="btn-group dropup">
                                      <button type="button" class="btn btn-primary dropdown-toggle px-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Options
                                      </button>
                                      <div class="dropdown-menu text-center bg-secondary">
                                        <!-- Dropdown menu links -->
                                        <button type="button" class="btn btn-primary col-sm-11" 
                                        onclick="edit('{{$user->id}}','{{$user->last_name}}', '{{$user->middle_name}}', '{{$user->first_name}}', '{{$user->birth_date}}', '{{$user->date_employed}}', '{{$user->gender}}', '{{$user->employment_status}}', '{{$user->employee_position}}', '{{$user->address}}', '{{$user->email_address}}', '{{$user->ec_name}}', '{{$user->ec_relation_to}}', '{{$user->ec_contact}}', '{{$user->ec_address}}')"
                                        >Edit</button><br>
                                        <button type="button" class="btn btn-success col-sm-11 mt-1" onclick="activate('{{$user->id}}')">Activate</button>
                                        <button type="button" class="btn btn-danger col-sm-11 mt-1" onclick="delete_user('{{$user->id}}')">Delete</button>
                                      </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>
                    <div class="card-footer bg-dark"></div>
                </div>
            </div>
        </div>
    </div>
     <div class="modal fade" role="dialog" id="image_shower">
          <div class="modal-dialog">
            <div class="modal-content bg-dark" style="width: 600px">
              <div class="modal-header">
                <div class="modal-title">
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body text-center">
                <div class="col-sm-12 h1"><label id="name"> </label></div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="h3" style="border: solid green; border-radius: 20px">Employee Info</div>
                        <div class="text-left text-light h5"> <label id="birthday1"></label></div>
                        <div class="text-left text-light h5"> <label id="employee_position1"></label></div>
                        <div class="text-left text-light h5"> <label id="date_employed1"></label></div>
                        <div class="text-left text-light h5"> <label id="employment_status1"></label></div>
                        <div class="text-left text-light h5"> <label id="email_address1"></label></div>
                        <div class="text-left text-light h5"> <label id="address1"></label></div>
                    </div>
                    <div class="col-sm-4">
                        <img src="" class="img-fluid" id="show_img_employee" style="width: 2in; height: 2in; object-fit: cover">
                    </div>
                    <div class="col-sm-12">
                        <div class="h3" style="border: solid green; border-radius: 20px">Emergency Contact</div>
                        <div class="text-left text-light h5"><label id="ec_name1">    </label></div>
                        <div class="text-left text-light h5"><label id="ec_relation_to1">    </label></div>
                        <div class="text-left text-light h5"><label id="ec_contact1">    </label></div>
                        <div class="text-left text-light h5"><label id="ec_address1">    </label></div>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

         <div class="modal fade" role="dialog" id="edit_modal">
              <div class="modal-dialog">
                <div class="modal-content" style="width: 600px; background-color: #F8F8FF">
                  <div class="modal-header">
                    <div class="modal-title">
                    Edit Employee
                    </div>
                    <button class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <form action="{{ route('employee.update') }}" id="update_btn" enctype="multipart/form-data" novalidate="" class="needs-validation">
                    <div class="row">
                  <div class="col-sm-4 form-group mb-3">
                    <input type="hidden" name="user_id" id="user_id">
                    <label>Lastname</label>
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Enter lastname">
                    <div class="invalid-feedback" id="err_last_name"></div>
                  </div>
                  <div class="col-sm-4 form-group mb-3">
                    <label>Middlename</label>
                    <input type="text" name="middle_name" id="middle_name" class="form-control" placeholder="Enter middlename">
                    <div class="invalid-feedback" id="err_middle_name"></div>
                  </div>
                  <div class="col-sm-4 form-group mb-3">
                    <label>Firstname</label>
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Enter firstname">
                    <div class="invalid-feedback" id="err_first_name"></div>
                  </div>
                  <div class="col-sm-6 form-group mb-3">
                    <label>Birthdate</label>
                    <input type="date" name="birth_date" id="birth_date" class="form-control" placeholder="Enter birthdate">
                    <div class="invalid-feedback" id="err_birth_date"></div>
                  </div>
                  <div class="col-sm-6 form-group mb-3">
                    <label>Date Employed</label>
                    <input type="date" name="date_employed" id="date_employed" class="form-control" placeholder="Enter Date Employed">
                    <div class="invalid-feedback" id="err_date_employed"></div>
                  </div>
                   <div class="col-sm-6 form-group mb-3">
                    <label>Gender</label>
                    <select name="gender" id="gender" class="form-control">
                      <option value="" selected="" disabled="">Select Gender</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                    <div class="invalid-feedback" id="err_gender"></div>
                  </div>
                  <div class="col-sm-6 form-group mb-3">
                    <label>Employment Status</label>
                    <select name="employment_status" id="employment_status" class="form-control">
                      <option value="" selected="" disabled="">Select Employment status</option>
                      <option value="Regular">Regular</option>
                      <option value="Trainee">Trainee</option>
                    </select>
                    <div class="invalid-feedback" id="err_employment_status"></div>   
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Employee Position</label>
                    <input type="text" name="employee_position" id="employee_position" class="form-control" placeholder="Enter Employee Position here">
                    <div class="invalid-feedback" id="err_employee_position"></div>
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Address</label>
                    <textarea class="form-control" id="address" name="address" placeholder="Enter Full Address here" rows="4"></textarea>
                    <div class="invalid-feedback" id="err_address"></div>  <hr> 
                  </div>
                  <div class="col-sm-12 form-group mb-3">
                    <label>Email</label>
                    <input type="text" name="email_address" id="email_address" class="form-control" placeholder="Enter Email">
                    <div class="invalid-feedback" id="err_email_address"></div>  
                  </div>
                  <div class="col-sm-12 form-group">
                    <label>Upload Profile Image</label>
                    <input type="File" name="user_img" id="user_img" class="form-control-file border">
                    <div class="invalid-feedback" id="err_user_img"></div> 
                    <hr>
                  </div>
                  <div class="col-sm-12 form-group text-center">
                    <label class="h5">------- Emergency Contact -------</label>
                  </div>
                  <div class="col-sm-12 form-group">
                    <label>Full Name</label>
                    <input type="text" name="ec_name" id="ec_name" class="form-control" placeholder="Enter Fullname here">
                    <div class="invalid-feedback" id="err_ec_name"></div> 
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Relationship to employee</label>
                    <input type="text" name="ec_relation_to" id="ec_relation_to" class="form-control" placeholder="Enter Relation to employee">
                    <div class="invalid-feedback" id="err_ec_relation_to"></div> 
                  </div>
                  <div class="col-sm-6 form-group">
                    <label>Contact Number</label>
                    <input type="text" name="ec_contact" id="ec_contact" class="form-control" placeholder="Enter Contact here">
                    <div class="invalid-feedback" id="err_ec_contact"></div> 

                  </div>
                  <div class="col-sm-12 form-group">
                    <label>Address</label>
                    <textarea name="ec_address" id="ec_address" class="form-control" placeholder="Enter Address here" rows="4"></textarea>
                    <div class="invalid-feedback" id="err_ec_address"></div> 
                  </div>
                </div>
                
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="clear_edit_button">Clear</button>
                    <button type="submit" class="btn btn-success">Update</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
@endsection
@section('script')
<script type="text/javascript">

  function activate(id){
    swal({
          title: "Are you sure?",
          text: "To Activate This Employee Account ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
          function(){
           $.ajax({
            type:"get",
            url:"{{route('employee.activate')}}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
              // console.log(response);
             if (response.status == true) {
                swal("Success", response.message, "success");
                setTimeout(function(){
               location.reload();
            }, 350);
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });
        });
    }


      function delete_user(id){
    swal({
          title: "Are you sure?",
          text: "To Delete This Employee Account ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
          function(){
           $.ajax({
            type:"delete",
            url:"{{route('employee.delete')}}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
              // console.log(response);
             if (response.status == true) {
                swal("Success", response.message, "success");
                setTimeout(function(){
               location.reload();
            }, 350);
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });
        });
    }


    function image_shower(img, name,  birth_date, date_employed, employment_status, email_address, address,employee_position,ec_name, ec_relation, ec_contact, ec_address){


        $('#image_shower').modal('show');
        $('#show_img_employee').attr('src', img);
        $('#name').html(name);
        $('#employee_position1').html('Employee position:  '+employee_position);
        $('#birthday1').html('Birth date:  '+birth_date);
        $('#date_employed1').html('Date employed:  '+date_employed);
        $('#employment_status1').html('Employment status:  '+employment_status);
        $('#email_address1').html('Email:  '+email_address);
        $('#address1').html('address:  '+address);
        $('#ec_name1').html('Name:  '+ec_name);
        $('#ec_relation_to1').html('Relation to employee:  '+ec_relation);
        $('#ec_contact1').html('Contact:  '+ec_relation);
        $('#ec_address1').html('Address:  '+ec_address);


    }
    function edit(id, lname, mname, fname, birthDate, dateEmployed, gender, employmentStatus, employeePosition, address, email, ecName, ecRelation, ecContact, ecAddress){
        $('#edit_modal').modal('show');
        $('#user_id').val(id);
        $('#last_name').val(lname);
        $('#last_name').val(lname);
        $('#middle_name').val(mname);
        $('#first_name').val(fname);
        $('#birth_date').val(birthDate);
        $('#date_employed').val(dateEmployed);
        $('#gender').val(gender);
        $('#employment_status').val(employmentStatus);
        $('#employee_position').val(employeePosition);
        $('#address').val(address);
        $('#email_address').val(email);
        $('#ec_name').val(ecName);
        $('#ec_relation_to').val(ecRelation);
        $('#ec_contact').val(ecContact);
        $('#ec_address').val(ecAddress);
    }

$('#update_btn').on('submit', function(e){
  e.preventDefault();
  // let formData = $(this).serialize();
  let form = $("#update_btn")[0];
    // Create an FormData object 
  var data = new FormData(form);
  let url = $(this).attr('action');
  $.ajax({
      type:"POST",
      url:url,
      data:data,
      enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
         //console.log(response);
       if (response.status === true) {
          swal("Success", response.message, "success");
          $('#update_btn')[0].reset();
          setTimeout(function(){
               location.reload();
            }, 400);
          
       }else{
        showValidator(response.error,'update_btn');
        console.log(response);
       }
      },
      error: function(error){
        console.log(error);
      }
    });

});

$('#clear_edit_button').on('click', function(){
     $('#update_btn')[0].reset();
});



</script>
@endsection

