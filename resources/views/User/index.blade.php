@extends('Layout.app_user')

@section('content')

<div class="content-wrapper">
    @include('Layout.bread', ['title' => 'Dashboard', 'data' => [] ])
    
	<div class="container-fluid mt-4">
        	<div class="row">
        		<div class="col-sm-12">
        			<div class="card bg-secondary">
        				<div class="card-header text-center h1 bg-dark">Quote of the Day</div>
        				<div class="card-body text-center">                		
                  		 <h2>{{ $quotes->quote_message }}</h2>            		
          				</div>
        				<div class="card-footer text-right bg-dark">
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
@endsection

