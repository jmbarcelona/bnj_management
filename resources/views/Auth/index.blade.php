@extends('Layout.app_auth')
@section('title', 'Login')
@section('content')
<div class="row justify-content-center mt-4">
<div class="login-box">
  <div class="login-logo">
  	<img src="{{ asset('img/logo.png')}}" style="width: 150px"><br>
    <a href="#"><b>BNJ</b>Santiago</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="{{ route('auth.check')}}" id="login_form" novalidate="" class="needs-validation">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" id="email_address" name="email_address">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
              <div class="invalid-feedback" id="err_email_address"></div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
              <div class="invalid-feedback" id="err_password"></div>

        </div>
        <div class="row">
          <div class="col-8">
            <p class="mb-0">
      </p>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mb-3">
        
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</div>
<!-- /.login-box -->
@endsection
@section('script')
<script type="text/javascript">
  $('#login_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    let url = $(this).attr('action');
    $.ajax({
        type:"post",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
           console.log(response);
         if (response.status === true) {
              window.location = response.redirect;
         }else{
          console.log(response);
          showValidator(response.error, 'login_form');
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });
	
</script>
@endsection