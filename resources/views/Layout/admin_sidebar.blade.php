<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('img/logo.png')}}" alt="AdminLTE Logo" class="" style=" width: 50px; height: 50px">
      <span class="brand-text text-warning">Brain IT Consultancy</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar ">
      <!-- Sidebar user panel (optional) -->
      <!-- SidebarSearch Form -->
      <div class="form-inline mt-4">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('dashboard.index')}}" class="nav-link">
              <i class="fas fa-tachometer-alt"></i>
              <p style="margin-left: 14px">
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fa-solid fa-users"></i>
              <p style="margin-left: 10px">
                Employee
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('employee.add')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('employee.index')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Current Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('employee.pass_employee') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Past Employee</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('attendance') }}" class="nav-link">
              <i class="fas fa-clipboard-user"></i>
              <p style="margin-left: 18px">
                Attendance
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('loan') }}" class="nav-link">
             <i class="fas fa-hand-holding-usd"></i>
              <p style="margin-left: 14px">
                Loan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('payroll') }}" class="nav-link">
              <i class="fas fa-file-invoice"></i>
              <p style="margin-left: 20px">
                Payroll
              </p>
            </a>
          </li> 
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
