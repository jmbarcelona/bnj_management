<!-- Navbar -->
  <nav class="navbar navbar-expand navbar-dark navbar-dark">
    <!-- Left navbar links -->
    <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="{{ asset('img/logo.png')}}" alt="AdminLTELogo" height="250" width="250">
  </div>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link h4 pb-0" href="#" role="button">BNJ<span class="text-orange">Santiago</span></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <!-- <a href="index3.html" class="nav-link">Home</a> -->
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <!-- <a href="#" class="nav-link">Contact</a> -->
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
      </li>
      
    </ul>
  </nav>
  <!-- /.navbar -->