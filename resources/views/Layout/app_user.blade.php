<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>

	<body class="hold-transition sidebar-mini layout-fixed">
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="{{ asset('img/logo.png')}}" alt="AdminLTELogo" height="250">
  </div>

			@include('Layout.user_sidebar')
			@include('Layout.user_navbar')
			
			@yield('content')
	</body>
	@include('Layout.footer')
	@yield('script')
</html>