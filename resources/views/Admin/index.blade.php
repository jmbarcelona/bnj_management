@extends('Layout.app_admin')
@section('title', 'Admin Dashboard')
@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$users}}</h3>

                <p>Present Today</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$users}}</h3>

                <p>Absent Today</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$users}}</h3>

                <p>Total Employees</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$users}}</h3>

                <p>Late Today</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>

        
        <div class="container-fluid mt-4">
        	<div class="row">
        		<div class="col-sm-12">
        			<div class="card bg-secondary">
        				<div class="card-header text-center h1 bg-dark">Quote of the Day</div>
        				<div class="card-body text-center">
                    @foreach($quotes as $quote)
                    <h2 id="quotes">{{ $quote->quote_message}}</h2>
                    @endforeach
          				</div>
        				<div class="card-footer text-right bg-dark">
        					<button type="button" class="btn btn-primary px-4" 
                  onclick="edit_quote()">Edit</button>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>

         <div class="modal fade" role="dialog" id="open_quote_edit">
              <div class="modal-dialog modal-lg">
                <div class="modal-content bg-dark">
                  <div class="modal-header">
                    <div class="modal-title h2">
                    <b>Edit Quote</b>
                    </div>
                    <button class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                  <form action="{{ route('dashboard.quote') }}" id="quote_form">
                    <div class="col-sm-12 form-group">
                      <input type="hidden" name="quote_id" id="quote_id" value="{{$quote->id}}" >
                      <textarea class="form-control bg-secondary text-xl" rows="7" id="quote_message" name="quote_message">{{$quote->quote_message}}</textarea>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-success" type="submit">Save</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
        
@endsection
@section('script')
<script type="text/javascript">
  function edit_quote(){
     $('#open_quote_edit').modal('show');
  }

  $('#quote_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    let url = $(this).attr('action');
    $.ajax({
        type:"get",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status === true) {
            swal("Success", response.message, "success");
            setTimeout(function(){
              location.reload();
            }, 500);
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });


  });
</script>
@endsection