@extends('Layout.app_admin')

@section('content')
<style>
    .btn_submit{
        border-radius: 0px;
    }
    form_width_sm{
        width: 20px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Attendance</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="background: #343A40; color: white;"><h3>Attendance Report</h3></div>
                <div class="card-body">
                    <div class="d-flex align-items-end">
                        <div class="col-md-4">
                            <label>Employees</label>
                            <input class="form-control py-2" type="search" placeholder="Search Employee" id="search" name="search">
                        </div>

                        <div class="col-md-2 col-sm-2">
                            <label>Year</label>
                            <input class="form-control py-2" type="number" placeholder="Year" id="year" name="year">
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <label>Month</label>
                            <select class="form-control" id="month" name="month">
                                <option value="">-- select month --</option>
                                <option disabled>------------------</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">Septemper</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-md-4 d-flex align-items-end justify-content-between">
                            <button class="btn btn-success btn_submit">Filter <i class="fas fa-filter"></i></button>

                        </div>
                    </div>

                    <br>
                    <div class="col-md-12">
                        <button class="btn btn-success btn_submit">Download <i class="fas fa-download"></i></button>
                        <br><br>

                    </div>
                    <div style="overflow-x: auto">
                        <table class="table table-hover" style="font-size: 11pt">
                            <thead>
                            <tr>
                                <th>Firstname</th>
                                <th>Date</th>
                                <th>Day</th>
                                <th><img src="{{ asset('img/icons/1QrpIt8klO-WSXOGLkFTnYnvTZF46S0wu-x4lZVMs.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1kG4cR663ocePqMwr7r17USRppu0ucoyZmQHMAy36.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1Bu-XdG8Ap6mxfk16QCFn6wpAojmOT2bjp6KwXxgP.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/11z7o1b9JusWokC7Y0kJFDNJy0jMFm3WGqiE_pq7f.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1YU6FJ9imXBk9-W_Na8N6igCkyXrf6Q5urgGd2VUT.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1HIlOSNCPkddmaIQc1yolyVesAIeDa0u7MgfDNPH7.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1T8ScrPPWtw0qicGNdKa9AqXibPlXO_4hsZhgmO79.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1d6i6ngXpZGIzQMI-Vq0AYodp93KLQWJejoHe5SjE.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1STS7CfBI97nL1_Qof3BoUrfeMgbtcZhBCgGHDaJQ.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1_vcDJjURIydhhqXr1n2CatQWBgJ4bz7ZHYYiQxwb.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1csJc6399bDNdVeVGH8PUvzN4lkYu2qmMEF0Lij5j.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/1FRJHJrukWR8UZodx4lhW1gf1PMcvxx5uH2B4aQdF.png') }}" height="40px"></th>
                                <th><img src="{{ asset('img/icons/15GG6meRaKIr0hNwWrPdeF03o_MbHa6Lz2YrTAvkY.png') }}" height="40px"></th>

                            </tr>
                            </thead>
                            <tbody>
{{--//////////////////////test lang <td>value here</td>////////////////////////////////////////////////////////////////////////--}}
                                    <?php
                                        $date = date('2022-10-1');
                                        $dates = new DateTime($date);
                                        $dayspost = new DateTime($date.'+ 30 day');

                                        while ($dates->format('Y-m-d') <= $dayspost->format('Y-m-d')){
                                            echo "<tr>
                                                  <td>JJ</td>
                                                  <td style=''>".$dates->format('Y-m-d')."</td>";
                                            if ($dates->format('l') == "Saturday" || $dates->format('l') == "Sunday"){
                                                echo "<td style='color: red'>".$dates->format('l')."</td>";
                                            }else{
                                                echo "<td style=''>".$dates->format('l')."</td>";
                                            }
                                        $dates = new DateTime($dates->format('Y-m-d').'+ 1 day');
                                            ?>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                    <?php
                                        echo "</tr>";
                                        }
                                    ?>
{{--/////////////////////////////////////////////////////////////////////////////////////////////////////////--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection