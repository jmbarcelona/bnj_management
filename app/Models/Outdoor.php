<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Outdoor extends Model
{
    use HasFactory;

    protected $table = 'outdoor';

    protected $fillable = [
    	'attendance_id',
    	'e_time_out',
    	'e_time_in',
    	'outdoor_reason',
    ];
}
