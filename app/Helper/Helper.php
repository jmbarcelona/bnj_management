<?php 

function monthsOption(){
	$months = [ 
		1 => 'January',
		2 => 'February',
		3 => 'March',
		4 => 'April',
		5 => 'May',
		6 => 'June',
		7 => 'July',
		8 => 'August',
		9 => 'Septemper',
		10 => 'October',
		11 => 'November',
		12 => 'December'];

	return $months;
}



function getFullName($data){
	return $data['firstname'].' '.$data['lastname'];
}


function getDailyRate($hour, $rate){
	return $hour * $rate;
}