<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Greetings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;


class DashboardController extends Controller
{
    public function index(Request $request){
    	$users = User::count();
    	$quotes = Greetings::get();
    	return view('Admin.index', compact('quotes'), compact('users'));
    }
    public function add_view(){
    	return view('Admin.addEmployee');
    }
    public function add_employee(Request $request){
		$last_name = $request->get('last_name');
		$middle_name = $request->get('middle_name');
		$first_name = $request->get('first_name');
		$birth_date = $request->get('birth_date');
		$date_employed = $request->get('date_employed');
		$gender = $request->get('gender');
		$employment_status = $request->get('employment_status');
		$address = $request->get('address');
		$email_address = $request->get('email_address');
		$password = $request->get('password');
		$confirm_pass = $request->get('confirm_pass');
		$user_img = $request->get('user_img');
		$ec_name = $request->get('ec_name');
		$ec_address = $request->get('ec_address');
		$ec_contact = $request->get('ec_contact');
		$ec_relation_to = $request->get('ec_relation_to');
	    $validator = Validator::make($request->all(), [
	            'last_name' => 'required',
	            'middle_name' => 'required',
	            'first_name' => 'required',
	            'birth_date' => 'required',
	            'date_employed' => 'required',
	            'gender' => 'required',
	            'employment_status' => 'required',
	            'address' => 'required',
	            'email_address' => 'required',
	            'password' => 'required',
	            'confirm_pass' => 'required|same:password',
	            'ec_name' => 'required',
	            'ec_address' => 'required',
	            'ec_contact' => 'required',
	            'ec_relation_to' => 'required'
	        ]);

	        if ($validator->fails()) {
	        	return response()->json(['status' => false, 'error' => $validator->errors()]);
	        }else{
	        	if (!empty($request->file('user_img'))) {
	        		$user = new User;
	        		$user->last_name = $last_name;
	        		$user->middle_name = $middle_name;
	        		$user->first_name = $first_name;
	        		$user->birth_date = $birth_date;
	        		$user->date_employed = $date_employed;
	        		$user->gender = $gender;
	        		$user->employment_status = $employment_status;
	        		$user->address = $address;
	        		$user->email_address = $email_address;
	        		$user->password = $password;
	        		$user->ec_name = $ec_name;
	        		$user->ec_address = $ec_address;
	        		$user->ec_contact = $ec_contact;
	        		$user->ec_relation_to = $ec_relation_to;
	        		$user->user_img = $productsData['user_img'] = $request->file('user_img')->store('user_img','public');
        		if ($user->save()) {
        			return response()->json(['status' => true, 'message' => 'Employee Added Successfully!']);
        			
        		}
	        	}else{
        			return response()->json(['status' => false, 'message' => 'Employee Image is required!']);

	        	}
	        }
   		}

    public function current_view(){
    	$users = User::where('user_type', 2)->get();
    	return view('Admin.current_employee', compact('users'));
    }

    public function quote(Request $request){
    	$quote_message = $request->get('quote_message');
    	$quote_id = $request->get('quote_id');
    	$quote = Greetings::where('id', $quote_id)->first();
    	$quote->quote_message = $quote_message;
    	if ($quote->save()) {
    		return response()->json(['status' => true, 'message' => 'Quote Updated Successfully']);
    	}
    	

    }
}