<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    public function index(){
    	return view('Auth.index');
    }

    public function check_login(Request $request){
    	$email_address = $request->get('email_address');
    	$password = $request->get('password');
    	$validator = Validator::make($request->all(), [
			'email_address' => 'required|email',
			'password' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			$redirect_url = '';
			$credentials = ['email_address' => $request->get('email_address'), 'password' => $request->get('password')];
			if (Auth::attempt($credentials)) {
				 /*If username and password is correct it will start and set the session or also known as Auth*/
				 $request->session()->regenerate();
				 $user = Auth::user(); /*$_SESSION['auth']*/
				 if ($user->user_type === 1) {
				 	$redirect_url = route('dashboard.index');
				 }else{
				 	$redirect_url = route('account.index');
				 }
				 return response()->json(['status' => true, 'redirect' => $redirect_url]);
			}else{
				$validator->errors()->add('password','Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);	
			}
		}
    }

    public function logoutUser(){
		Session::flush();
    	Auth::logout();
		return redirect(route('login'));
	}

}
