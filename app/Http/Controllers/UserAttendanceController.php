<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Outdoor;
use Auth;
use Validator;


class UserAttendanceController extends Controller
{
    public function index(){
    	return view('UserAttendance.index');
    }

    public function list(){
    	$attendance = Attendance::where('user_id', Auth::id())->get();
    	return response()->json(['status' => true, 'data' => $attendance]);
    }

    public function insert(){
		$d=cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
 		$exist = Attendance::whereMonth('attendance_date', date('m'))->first();
 		$count = 1;

    	if (empty($exist)) {
    		while ($count <= $d) {
    			$datetoday = date(date("Y").'-'.date("m").'-'.$count);
    			$attendance = new Attendance;
                $attendance->attendance_date = $datetoday;
                $attendance->user_id = Auth::id();
                $attendance->status = 1;
    			$attendance->save();
    			$count++;
    		}
    	}else{
    		return response()->json(['status' => false, 'message' => 'This month table already generated']);
    	}
    		return response()->json(['status' => true, 'message' => 'You inserted']);

    	

    }

    public function timeInAm(Request $request){
    	$time_in_am = $request->get('time');
    	$id = $request->get('id');
    	
    	$attendance = attendance::where('id', $id)->first();
    	$attendance->time_in_am = $time_in_am;
    	if ($attendance->save()) {
    		return response()->json(['status' => true, 'message' => 'Time in successfully!']);
    	}
    }

    public function timeOutAm(Request $request){
    	$time_out_am = $request->get('time');
    	$id = $request->get('id');
    	
    	$attendance = attendance::where('id', $id)->first();
    	$attendance->time_out_am = $time_out_am;
    	if ($attendance->save()) {
    		return response()->json(['status' => true, 'message' => 'Time in successfully!']);
    	}
    }

    public function timeInPm(Request $request){
    	$time_in_pm = $request->get('time');
    	$id = $request->get('id');
    	$attendance = attendance::where('id', $id)->first();
    	$attendance->time_in_pm = $time_in_pm;
    	if ($attendance->save()) {
    		return response()->json(['status' => true, 'message' => 'Time in successfully!']);
    	}
    }

    public function timeOutPm(Request $request){
    	$time_out_pm = $request->get('time');
    	$id = $request->get('id');
    	$attendance = attendance::where('id', $id)->first();
    	$attendance->time_out_pm = $time_out_pm;
    	if ($attendance->save()) {
    		return response()->json(['status' => true, 'message' => 'Time in successfully!']);
    	}
    }

    public function outdoorList($id){
    	$outdoor = Outdoor::where('attendance_id', $id)->get();
    	return response()->json(['status' => true, 'data' => $outdoor]);
    }

    public function outdoorTimeOut(Request $request){
    	$attendance_id = $request->get('attendanceId');
    	$time_out_outdoor = $request->get('time_out');
    	$outdoor = new Outdoor;
    	$outdoor->attendance_id = $attendance_id;
    	$outdoor->e_time_out = $time_out_outdoor;
    	$outdoor->save();
    	return response()->json(['status' => true, 'message' => 'You can now go out, Please take care!']);	
    }


    public function editOutdoorTimeOut(Request $request){
    	$outdoor_id = $request->get('outdoor_id');
    	$time_out = $request->get('time_out');
    	$time_out_outdoor = Outdoor::where('id', $outdoor_id)->first();
    	$time_out_outdoor->e_time_out = $time_out;
    	$time_out_outdoor->save();
    	return response()->json(['status' => true, 'message' => 'Time out edited']);

    	
    }
    public function editOutdoorTimeIn(Request $request){
    	$outdoor_id = $request->get('outdoor_id');
    	$time_in = $request->get('time_in');
    	$time_in_outdoor = Outdoor::where('id', $outdoor_id)->first();
    	$time_in_outdoor->e_time_in = $time_in;
    	$time_in_outdoor->save();
    	return response()->json(['status' => true, 'message' => 'Time in inserted']);
    }

    public function outdoorReason(Request $request){
        $attendance_id = $request->get('attendance_id');
        $reason = $request->get('reason');

        $outdoor = Outdoor::where('id', $attendance_id)->first();
        $outdoor->outdoor_reason = $reason;
        $outdoor->save();
        return response()->json(['status' => true, 'message' => 'outdoor inserted']);
    }

    public function workPlace(Request $request){
        $attendance = Attendance::where('id', $request->get('id'))->first();
        $attendance->status = $request->get('data');
        $attendance->save();
        return response()->json(['status' => true, 'message' => 'tama ka na andrew']);

    }

    public function addReport(Request $request){
        $validator = Validator::make($request->all(), [
                'report_message' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'error' => $validator->errors()]);
            }else{
               $attendance =  Attendance::where('id', $request->get('attendance_id_report'))->first();
               $attendance->report_message = $request->get('report_message');
               if (!empty($request->file('report_file'))) {
                $attendance->report_file = $request->file('report_file')->store('daily_report','public');
               }
               $attendance->save();
               return response()->json(['status' => true, 'message' => 'pasok sa banga!']);
            }

    }

    public function addReason(Request $request){
        $validator = Validator::make($request->all(), [
                'reason_message' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'error' => $validator->errors()]);
            }else{
               $attendance =  Attendance::where('id', $request->get('attendance_id_reason'))->first();
               $attendance->reason = $request->get('reason_message');
               $attendance->save();
               return response()->json(['status' => true, 'message' => 'pasok din to sa banga!']);

            }
    }
}





                                       
                                            
