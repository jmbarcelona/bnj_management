<?php

namespace App\Http\Controllers;
use App\Models\Greetings;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(){
    	$quotes = Greetings::select('quote_message')->where('id', 1)->first();
        return view('User.index', compact('quotes'));
    }
}
